import { Component } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { FlightService } from './flight.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  flight: any;
  flightNum: any;
  colorTheme = 'theme-blue';
  flightList: Flight[] = [];
  errorResponse: string;
  success: boolean;

  bsConfig: Partial<BsDatepickerConfig>;

  constructor(private flightService: FlightService) {
    this.flight = {};
    this.flightNum = {};
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });

    this.getAllFlights();
  }

  getFlightDetails() {
    this.success = false;
    let departs = (this.flight.departs).toJSON();
    this.flightService.getFlightDetails(this.flight, departs).subscribe((response: Flight[]) => {
      this.flightList = response;
      if (this.flightList.length > 0) {
        this.success = true;
      } else {
        this.success = false;
        this.errorResponse = "No records Found!"
      }
      console.log(response);
    })
  }

  getFlightDetailsByNumber() {
    this.success = false;
    let departs = (this.flightNum.departs).toJSON();
    this.flightService.getFlightDetailsByNumber(this.flightNum, departs).subscribe((response: Flight[]) => {
      this.flightList = response;
      if (this.flightList.length > 0) {
        this.success = true;
      } else {
        this.success = false;
        this.errorResponse = "No records Found!"
      }
      console.log(response);
    })
  }

  getAllFlights() {
    this.flightService.getAllFlights().subscribe((response: Flight) => {

      console.log(response);

      // this.flightList = response;
    },
      error => {
        console.log("error came", error);
      });
  }

  refreshData() {
    this.flight = {};
  }
}


export interface Flight {
  id: number;
  name: string;
  aircraft: string;
  arrival: Date;
  carrier: string;
  departure: Date;
  destination: string;
  distance: 925
  flightNumber: string;
  origin: string;
  status: string;
  travelTime: string;
}