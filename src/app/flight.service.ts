import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Flight } from './app.component';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  constructor(private http: HttpClient) { }

  getFlightDetails(flight: any, departs: any): Observable<Flight[]> {
    return this.http.get<Flight[]>('/api/flight/' + flight.origin + '/' + flight.destination + '/' + departs);
  }

  getFlightDetailsByNumber(flight: any, departs: any): Observable<Flight[]> {
    return this.http.get<Flight[]>('/api/flight/number/' + flight.flightNumber + '/' + departs);
  }

  getAllFlights(): Observable<Flight> {
    return this.http.get<Flight>('/api/flights');
  }
}
